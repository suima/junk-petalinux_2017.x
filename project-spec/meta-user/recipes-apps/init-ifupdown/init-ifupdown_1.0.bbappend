# Set static IP address

LICENSE = "MIT"
SRC_URI = "file://Makefile \
           file://interfaces \
          "

S = "${WORKDIR}"

do_compile() {
        oe_runmake
}
do_install() {
        install -m 0644 ${WORKDIR}/interfaces ${D}${sysconfdir}/network/interfaces
}
